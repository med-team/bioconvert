Metadata-Version: 1.1
Name: bioconvert
Version: 0.2.0
Summary: convert between bioinformatics formats
Home-page: ['http://pypi.python.org/pypi/bioconvert']
Author: Thomas Cokelaer
Author-email: thomas.cokelaer@pasteur.fr
License: BSD
Download-URL: ['http://pypi.python.org/pypi/bioconvert']
Description-Content-Type: UNKNOWN
Description: Bioconvert
        ==========
        
        Bioconvert is a project to facilitate the interconversion of life science data from one format to another.
        
        .. image:: https://badge.fury.io/py/bioconvert.svg
            :target: https://pypi.python.org/pypi/bioconvert
        
        .. image:: https://secure.travis-ci.org/biokit/bioconvert.png
            :target: http://travis-ci.org/biokit/bioconvert
        
        .. image:: https://coveralls.io/repos/github/biokit/bioconvert/badge.svg?branch=master
            :target: https://coveralls.io/github/biokit/bioconvert?branch=master
        
        .. image:: http://readthedocs.org/projects/bioconvert/badge/?version=master
            :target: http://bioconvert.readthedocs.org/en/latest/?badge=master
            :alt: Documentation Status
        
        .. image:: https://badges.gitter.im/biokit/bioconvert.svg
            :target: https://gitter.im/bioconvert/Lobby?source=orgpage
        
        
        :note: Bioconvert is tested with Travis for the following Python version: 3.5 and 3.6. Python 2 won't be provided.
        
        :contributions: Want to add a convertor ? Please join https://github.com/biokit/bioconvert/issues/1
        :issues: Please use https://github.com/biokit/bioconvert/issues
        
        
        Installation
        ###############
        
        In order to install bioconvert, you can use **pip**::
        
            pip install bioconvert
        
        We also provide releases on bioconda (http://bioconda.github.io/)::
        
            conda install bioconvert
        
        and Singularity container are available. See
        http://bioconvert.readthedocs.io/en/master/user_guide.html#installation for
        details.
        
        Usage
        ##########
        
        ::
        
            bioconvert input.fastq output.fasta
            bioconvert input.fq output.fasta
            bioconvert input.mybam output.bed --input-format bam
            bioconvert --formats
            bioconvert --help
        
        Available Converters
        #######################
        
        .. image:: https://raw.githubusercontent.com/biokit/bioconvert/master/doc/conversion.png
            :width: 80%
        
        
        
        
        
        
        
        
        
        
        
        
Keywords: NGS,bam2bed,fastq2fasta,bam2sam
Platform: Linux
Platform: Unix
Platform: MacOsX
Platform: Windows
Classifier: Development Status :: 1 - Planning
Classifier: Intended Audience :: Developers
Classifier: Intended Audience :: Science/Research
Classifier: License :: OSI Approved :: BSD License
Classifier: Operating System :: OS Independent
Classifier: Programming Language :: Python :: 3
Classifier: Programming Language :: Python :: 3.4
Classifier: Programming Language :: Python :: 3.5
Classifier: Programming Language :: Python :: 3.6
Classifier: Topic :: Software Development :: Libraries :: Python Modules
Classifier: Topic :: Scientific/Engineering :: Bio-Informatics
Classifier: Topic :: Scientific/Engineering :: Information Analysis
